document.getElementById("cameraTakePicture").addEventListener("click", cameraTakePicture);
document.getElementById("deletePicture").addEventListener("click", deletePicture);
document.getElementById("lanzarToast").addEventListener("click", lanzarToast);

/*
function verificar() {
    var resultado = document.getElementById("txtNombre").value;
    document.getElementById("txtResultado").value = resultado;
    console.log("Ok");
}
*/
function cameraTakePicture() {

    navigator.camera.getPicture(onSuccess, onFail, {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        saveToPhotoAlbum: false
    });

    function onSuccess(imageData) {
        var image = document.getElementById('miImagen');
        image.src = "data:image/jpeg;base64," + imageData;
        alert("Base 64:   " + imageData)
    }

    function onFail(message) {
        alert('Ocurrió un error: ' + message);
    }
}

function deletePicture() {
    var image = document.getElementById('miImagen');
    image.src = "";
    var patron = [500, 200, 500, 200, 500];
    navigator.vibrate(patron);
}

function lanzarToast() {
    window.plugins.toast.showWithOptions({
            message: "Hola Mundo",
            duration: 3000,
            position: "bottom"
        },
        onSuccess, // optional
        onError // optional
    );
}